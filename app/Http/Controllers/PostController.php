<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {    
        //lấy tất cả
        //  $post = Post::all();



        //lấy giá trị sắp xếp theo trường name
         //desc giảm dần
        //  $post = Post::orderBy('name','desc')->get();
         


         //lấy giá trị theo điều kiện 
        //  $post = Post::where('name','black')->get();
     


        //lấy giá trị theo ngày gần nhất
         $post = Post::latest()->get();
        return view('posts.index',['posts' => $post]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $post = new Post();
        $post->name = request('name');
        $post->type = request('type');
        $post->price = request('price');
        $post->ingredients =request('ingredients');
        $post-> save();
      
   
        return redirect('/')->with('mssg','Create succetful');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $post = Post::findOrFail($id);
        return view('posts.details',['post'=>$post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $post = Post::findOrFail($id);
       $post ->delete();
       return redirect('/post');
    }
}
