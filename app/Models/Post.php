<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
   //table name
   protected $table ='posts';
   //primarykey
   protected $primary = 'item_id';
   //timestamps
   public $timestamps = false;
   
   protected $casts =[
   'ingredients' => 'array'
   ];

   protected $fillable =[
      'id', 'name', 'price', 'type'
   ];

}


