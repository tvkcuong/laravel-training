@extends('posts.layouts.app')

@section('content')


      <p>This is the index pages.</p>
      <h3>Diamond list</h3>
      @foreach($posts as $post)
      <a href="/post/{{$post->id}}"  >
        <div><h3>{{ $post->id }}:</h2>{{ $post->name }} -- {{ $post->price }} --type: {{ $post->type }}</div>
        <p class="ingredients">Extra ingredients:</p>
        <ul>
          @foreach($post-> ingredients as $idg)
          <li>
         {{$idg}} 
          </li>
          @endforeach
        </ul>
        </a>
        <form action="/post/{{$post->id}}" method= "POST">
      @csrf
      @method('DELETE')
      <button>Delete</button>
      </form>
      @endforeach
      <a href="/post/create">Create new post</a>
@endsection