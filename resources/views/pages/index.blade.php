@extends('pages.layouts.app')

@section('content')
      <h1>{{$title}}</h1>
      <p>This is the index pages.</p>
      <h3>Diamond list</h3>
      @foreach($diamonds as $diamond)
        <div>{{ $loop->index }} : {{ $diamond['name'] }} -- {{ $diamond['price'] }}</div>
      @endforeach
@endsection
