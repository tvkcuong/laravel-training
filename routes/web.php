<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PostController;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
     $type = request('type');
    return view('main',[
    'type' => $type
    ]);
});
Route::get('/index', [PageController::class, 'index']);
Route::get('/about',[PageController::class, 'about'] );
Route::get('/service',[PageController::class, 'service'] );

 
Route::get('/post',[PostController::class,'index'])->middleware('auth');

Route::get('/post/create',[PostController::class,'create']);
Route::get('/post/{id}',[PostController::class,'show'])->middleware('auth');
Route::post('/post',[PostController::class,'store'])->middleware('auth');
Route::delete('/post/{id}',[PostController::class,'destroy'])->middleware('auth');


Route::get('/hello', function () {
    return '<h1> Hello world!</h1>';
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');



