## Setup

 1. Tạo mới file **.env**, copy nội dung **.env.example** sang và thay đổi giá trị ``DB_DATABASE`` thành database của mình
 2. Chạy ``composer install`` để cài đặt các package **composer**
 3. Chạy ``php artisan key:generate`` để sinh key cho ứng dụng
 4. Chạy ``php artisan migrate`` để tạo các bảng trong database
 5. Chạy ``php artisan db:seed`` để sinh dữ liệu cho database
 6. Chạy ``php artisan passport:install --force`` để tạo key cho **passport**
 7. Chạy ``php artisan l5-swagger:generate`` để tạo api document
 8. Chạy ``php artisan jwt:secret`` để tạo key cho jwt

## Update
 1. Checkout branch ``develop`` và pull code mới nhất về
 2. Chạy ``composer dump-autoload`` để autoload các class mới
 3. Chạy ``php artisan migrate:refresh --seed`` để refresh lại database
 4. Chạy ``php artisan passport:install --force`` để tạo key cho **passport**
 5. Chạy ``php artisan l5-swagger:generate`` để tạo api document

## Run

Chạy ``php artisan serve`` để khởi động serve
Truy cập ``/api/documentation`` để sử dụng api document

## Tool generate code

Chạy ``php artisan generate:api {MODEL}`` để sinh code api
Chạy ``php artisan generate:nuxt {MODEL}`` để sinh code nuxt
Chạy ``php artisan generate:all`` để sinh code tất cả
Chạy ``php artisan vendor:publish --provider="Digidinos\CodeGenerator\CodeGeneratorServiceProvider" --force`` để publish template

## Firebase

Tải extension [php_grpc](https://pecl.php.net/package/gRPC/1.31.0/windows) (chọn đúng version với PHP hiện tại), sao chép vào thư mục ```ext``` trong PHP

## Setup cron
* * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1

https://gist.github.com/vrajroham/6565c4b2e9b4db693c1524394545a610


## Debug

  1. Thông báo lỗi: ``Personal access client not found. Please create one.``
  > Giải pháp: Chạy ``php artisan passport:install --force``

  2. Thông báo lỗi: ``Unable to flush cache.`` (Laravel Permission)
  > Giải pháp: Chạy ``sudo php artisan cache:forget spatie.permission.cache && sudo php artisan cache:clear``

## SQL Server
Tải [ODBC Driver](https://docs.microsoft.com/en-us/sql/connect/odbc/download-odbc-driver-for-sql-server?view=sql-server-ver15)

Tải extension [sqlsrv](https://docs.microsoft.com/en-us/sql/connect/php/download-drivers-php-sql-server?view=sql-server-ver15)